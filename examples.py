from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
from keras.preprocessing import image
import librosa
import numpy as np

n_mels = 224
length = 224

y, sr = librosa.load("/Users/Joost/Downloads/FreeSound/subset/Laughter/0ba8bcc2.wav")
S = librosa.feature.melspectrogram(y=y, sr=sr, n_mels=n_mels)
T = S[:, 0:length] # select first 128 samples to create a 128 x 128 patch

hist_img = np.zeros((n_mels, length, 3))
hist_img[:, :, 0] = T
hist_img[:, :, 1] = T
hist_img[:, :, 2] = T
hist_img = np.expand_dims(hist_img, axis=0)

#img_path = "/Users/Joost/Pictures/mdlogo.jpg"
#img = image.load_img(img_path, target_size=(224, 224))
#x = image.img_to_array(img)
#x = np.expand_dims(x, axis=0)

model = VGG16( include_top=True, weights='imagenet', input_tensor=None, input_shape=None, pooling=None, classes=1000)
features = model.predict(hist_img)
print(features.shape)

# Finetune VGG?
