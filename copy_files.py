import shutil
import csv
import os

class FileCopier(object):

    def copy(self, csv_path, sample_path, destination_path):
        with open(csv_path, 'r') as input_file:
            filereader = csv.reader(input_file, delimiter=',', quotechar='"')
            for row in filereader:
                src = os.path.join(sample_path, row[1])
                dst_dir = os.path.join(destination_path, row[2])
                dst_file = os.path.join(dst_dir, row[1])
                if not os.path.exists(dst_dir):
                    os.mkdir(dst_dir)
                shutil.copyfile(src, dst_file)


if __name__ == "__main__":
    fc = FileCopier()
    fc.copy("/Users/Joost/git/md-audio-classification-hackaton/labels.csv", "/Users/Joost/Downloads/FreeSound/audio_train", "/Users/Joost/Downloads/FreeSound/subset")