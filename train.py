import logging
import time
import random
import os
import numpy as np
import librosa
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.applications.vgg16 import VGG16
from score_handler import LoggingScoreHandler, PlottingScoreHandler


class LaughterOrViolinClassifier(object):

    def __init__(self, num_vgg_classes, score_handlers=[]):
        self.score_handlers = score_handlers
        self.logger = logging.getLogger(self.__class__.__name__)
        self.num_vgg_classes = num_vgg_classes
        self.vgg_model = VGG16(include_top=True, weights='imagenet', input_tensor=None, input_shape=None, pooling=None,
                      classes=self.num_vgg_classes)

    def train_model(self, compiled_model, training_samples, test_samples, batch_size, num_epochs=1, shuffle=True):
        for epoch in range(0, num_epochs):
            self.logger.info("Starting epoch %d" % epoch)
            random.shuffle(training_samples)
            for batch_start in range(0, len(training_samples), batch_size):
                batch = training_samples[batch_start:batch_start + batch_size]
                features, labels = self.create_feature_batch(batch)
                self.logger.info("Train on %d samples" % labels.shape[0])
                scores = compiled_model.train_on_batch(features, labels)
                self.on_batch_processed(scores[0], scores[1])
            test_accuracy = self.evaluate_model(compiled_model, test_samples, num_bins, batch_size)
            self.on_epoch_evaluated(epoch, test_accuracy)
        self.on_training_done()
        return compiled_model

    def create_feature_batch(self, batch):
        batch_size = len(batch)
        features = np.zeros((batch_size, self.num_vgg_classes), dtype=np.float32)
        labels = np.zeros((batch_size, 1), dtype=np.int8)
        for i, sample in enumerate(batch):
            try:
                features[i, :] = self.create_audiovgg_features(sample[0])
                labels[i] = sample[1]
            except (ValueError, IOError):
                # batch = np.delete(batch, i, 0)
                pass
        return features, labels

    def create_audiovgg_features(self, path, num_mels=224, samples=224):
        y, sr = librosa.load(path)
        spect = librosa.feature.melspectrogram(y=y, sr=sr, n_mels=num_mels)
        if np.size(spect, 1) < samples:
            raise ValueError("Number of samples < {}".format(samples))
        else:
            spect_slice = spect[:, 0:samples]
            hist_img = np.zeros((num_mels, samples, 3))
            hist_img[:, :, 0] = spect_slice
            hist_img[:, :, 1] = spect_slice
            hist_img[:, :, 2] = spect_slice
            hist_img = np.expand_dims(hist_img, axis=0)
            features = self.vgg_model.predict(hist_img)
            return features

    @staticmethod
    def split_dataset(pos_samples, neg_samples, train_ratio=0.8, shuffle=True):
        if shuffle:
            random.shuffle(pos_samples)
            random.shuffle(neg_samples)
        length = min(len(pos_samples), len(neg_samples))
        pos_samples = pos_samples[:length]
        neg_samples = neg_samples[:length]
        training = []
        test = []
        for i, pos_sample in enumerate(pos_samples):
            neg_sample = neg_samples[i]
            if i < length * train_ratio:
                training.append(pos_sample)
                training.append(neg_sample)
            else:
                test.append(pos_sample)
                test.append(neg_sample)
        return training, test

    @staticmethod
    def label_paths(path, label):
        result = []
        for (dirpath, dirnames, filenames) in os.walk(path):
            for filename in filenames:
                if filename.endswith((".wav")):
                    result.append((os.path.join(dirpath, filename), label))
            break
        return result


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    pos_paths = LaughterOrViolinClassifier.label_paths("/Users/Joost/Downloads/FreeSound/subset/Laughter", 1)
    neg_paths = LaughterOrViolinClassifier.label_paths("/Users/Joost/Downloads/FreeSound/subset/Violin_or_fiddle", 0)
    train, test = LaughterOrViolinClassifier.split_dataset(pos_paths, neg_paths)
    timestamp = int(time.time())
    plot_file = "/tmp/plot_{}.pdf".format(timestamp)
    handlers = [LoggingScoreHandler(), PlottingScoreHandler(plot_file, 1)]
    num_vgg_classes = 1000
    classifier = LaughterOrViolinClassifier(num_vgg_classes, handlers)
    model = Sequential()
    model.add(Dense(64, input_dim=num_vgg_classes, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])
    trained_model = classifier.train_model(model, train, test, 100, 10)