import logging
import abc
import sys
from matplotlib import pyplot as plt


class AbstractScoreHandler(object):

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def on_training_loss(self, loss):
        raise NotImplementedError()

    @abc.abstractmethod
    def on_training_accuracy(self, accuracy):
        raise NotImplementedError()

    @abc.abstractmethod
    def on_test_accuracy(self, accuracy, epoch=None):
        raise NotImplementedError()

    @abc.abstractmethod
    def on_done(self):
        raise NotImplementedError()


class LoggingScoreHandler(AbstractScoreHandler):

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)

    def on_training_loss(self, loss):
        self.logger.info("Training Loss    : %s" % str(loss))

    def on_training_accuracy(self, accuracy):
        self.logger.info("Training Accuracy: %s" % str(accuracy))

    def on_test_accuracy(self, accuracy, epoch=None):
        self.logger.info("Test Accuracy {}: {:.6f}".format(("" if epoch is None else "[" + str(epoch) + "]"), accuracy))
        # https://stackoverflow.com/questions/4098131/how-to-update-a-plot-in-matplotlib

    def on_done(self):
        self.logger.info("Done")


class PlottingScoreHandler(AbstractScoreHandler):

    def __init__(self, path, epoch_plot_freq=sys.maxsize):
        self.path = path
        self.epoch_plot_freq = epoch_plot_freq
        self.training_loss = []
        self.training_accuracy = []
        self.test_accuracy = []

    def on_training_loss(self, loss):
        self.training_loss.append(loss)

    def on_training_accuracy(self, accuracy):
        self.training_accuracy.append(accuracy)

    def on_test_accuracy(self, accuracy, epoch=None):
        self.test_accuracy.append(accuracy)
        if (epoch + 1) % self.epoch_plot_freq == 0:
            self.plot_all()

    def on_done(self):
        self.plot_all()

    def plot_all(self):
        # Training Loss
        plt.subplot(2, 2, 1)
        plt.ylabel("Training Loss")
        plt.plot(self.training_loss)
        # Training Accuracy
        plt.subplot(2, 2, 2)
        plt.ylabel("Training Accuracy")
        plt.plot(self.training_accuracy)
        # Evaluation Accuracy
        plt.subplot(2, 2, 3)
        plt.ylabel("Test Accuracy")
        plt.plot(self.test_accuracy)
        plt.savefig(self.path, orientation="landscape")
